#!/usr/bin/env bash
#
# shadowsocks - this script install shadowsocks automatic
#
# chkconfig:   - 85 15
# description:  shadowsocks is an proxy agent server, HTTP(S) reverse \
#               proxy and IMAP/POP3 proxy server
# install shadowsocks software

# install env
yum install python-setuptools && easy_install pip

# install
pip install shadowsocks

#start shadowsocks server side
ssserver -p 10934 -k o8Qz98M9nL8G866n -m aes-256-cfb --user nobody -d start

#check status
netstat -antp | grep 10934

echo "done!"


#client side
#download shadowsocks.exe; then add your server info.