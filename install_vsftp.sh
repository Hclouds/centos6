#!/bin/bash
#
# install_vsftp.sh - this script install vsftp automatic
#
# chkconfig:   - 85 15
# description: how to install vsftp soft
#

# install basic soft
yum -y install vsftpd db4 db4-utils

# create user and group
useradd -s /sbin/nologin -d /home/vsftpuser vsftpuser

# here need you to do manualy
if false; then

#此处的代码不回执行
vim /etc/vsftpd/vsftpd.conf

#修改行:
12 anonymous_enable=NO
52 xferlog_file=/var/log/vsftpd.log
81 ascii_upload_enable=YES
82 ascii_download_enable=YES
97 chroot_list_enable=YES
99 chroot_list_file=/etc/vsftpd/chroot_list
117 pam_service_name=vsftpd

#添加:
guest_enable=YES
guest_username=vsftpuser
user_config_dir=/etc/vsftpd/vuser_conf

#add ftp user
echo -e "xudong\nabc123" >> /etc/vsftpd/vuser_passwd.txt

# create pwd file
db_load -T -t hash -f /etc/vsftpd/vuser_passwd.txt /etc/vsftpd/vuser_passwd.db


# set auth file
vim /etc/pam.d/vsftpd
#32位系统
auth required pam_userdb.so db=/etc/vsftpd/vuser_passwd
account required pam_userdb.so db=/etc/vsftpd/vuser_passwd

#64位系统
auth required /lib64/security/pam_userdb.so db=/etc/vsftpd/vuser_passwd
account required /lib64/security/pam_userdb.so db=/etc/vsftpd/vuser_passwd

# create user dir
mkdir /etc/vsftpd/vuser_conf/ && echo -e "local_root=/tmp/ftp/xudong\nwrite_enable=YES\nanon_world_readable_only=NO\nanon_upload_enable=YES\nanon_mkdir_write_enable=YES\nanon_other_write_enable=YES\nlocal_umask=022\nanon_umask=022" > /etc/vsftpd/vuser_conf/xudong

mkdir -p /tmp/ftp/xudong && chown vsftpuser.vsftpuser -R /tmp/ftp

#允许指定的用户更改目录
echo xudong >>/etc/vsftpd/chroot_list

#close selinux and iptables
#setenforce 0
#/etc/init.d/iptables stop

fi

#restart vsftpd
/etc/init.d/vsftpd restart

# check status
netstat -antp | grep vsftp

# client test login
# ftp 192.168.1.12