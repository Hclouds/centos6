#!/bin/bash
#
# samba - this script install samba automatic
#
# chkconfig:   - 85 15
# description:  Samba is an File sahre server, HTTP(S) reverse \
#               proxy and IMAP/POP3 proxy server
# install samba software
yum -y install samba samba-common samab-client

# restart samba service
/etc/init.d/smb restart


# back your samba config file
cp /etc/samba/smb.conf /etc/samba/smb_original.conf


#案例一: 创建/usr/local/nginx/html/sambshare共享,所有人可访问读写
vim /etc/samba/smb.conf
	#修改
	74 workgroup = WORKGROUP
	101 security = share

	#添加
	[sambshare]
		comment = Public share Stuff
		path = /usr/local/nginx/html/sambshare
		public = yes
		browseable = yes
		writable = yes

#创建目录和修改权限
mkdir /usr/local/nginx/html/sambshare
chown -R nobody.nobody /usr/local/nginx/html/sambshare


#案例二: 创建/usr/local/nginx/html/usershare共享，指定用户通过密码访问
vim /etc/samba/smb.conf
    #修改
    74 workgroup = WORKGROUP
	101 security = user

    #添加
    [usershare]
        comment = user share
        path = /usr/local/nginx/html/usershare
        browseable = yes
        writeable = yes
        write list = www #@www

#添加samba用户，并设置密码
smbpasswd -a www
smbpasswd -x www #删除用户


#测试samba配置是否正确
testparm

# 重启服务
/etc/init.d/smb restart #支持centos6
systemctl restart smb #支持centos7

# windows测试
//192.167.1.12/sambshare



#linux访问共享
smbclient //192.168.1.12/sharename -U sabrinna

#linux挂载共享文件夹: 支持以前的centos版本
mount -t smbfs -o usernmae=xudong,password=abc123  //192.168.1.12/share /mnt/share

#linux挂载共享文件夹: 支持centos7.0
mount -t cifs -o usernmae=xudong,password=abc123  //192.168.1.12/share /mnt/share


#可能遇到的问题
#开机自动挂载
#挂载windows目录遇到中文乱码?
    vim /etc/sysconfig/i18n #添加如下内容:
        LANG="zh_CN.GB18030"
        SUPPORTED="zh_CN.UTF-8:zh_CN:zh:en_US.UTF-8:en_US:en:zh_CN.GB18030"
        SYSFONT="latarcyrheb-sun16"

    #执行:
    source /etc/sysconfig/i18n

