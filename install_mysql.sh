#!/bin/bash
#
# install_mysql.sh - this script install mysql automatic
#
# chkconfig:   - 85 15
# @author: xudong7930 <xudong7930@gmail.com>
# description:  MYSQL is an DATABASE server, DATABASE reverse \
#               used to store data

# add user and group
groupadd mysql && useradd -s /sbin/nologin -g mysql mysql
mkdir -p /usr/local/mysql/data
rm -fr /etc/my.cnf

# install basic tools
yum install gcc gcc-c++ cmake make ncurses-devel bison

# download and install
# sohu mysql mirror: http://mirrors.sohu.com/mysql
wget http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.29.tar.gz

tar xvf mysql-5.6.29.tar.gz

cd mysql-5.6.29

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DMYSQL_DATADIR=/usr/local/mysql/data -DMYSQL_UNIX_ADDR=/usr/local/mysql/mysql.sock -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_MEMORY_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DENABLED_LOCAL_INFILE=1 -DMYSQL_USER=mysql -DENABLED_LOCAL_INFILE=1 -DENABLE_DOWNLOADS=1 -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DEXTRA_CHARSETS=all -DMYSQL_TCP_PORT=3306

make && make install

chown -Rf mysql:mysql /usr/local/mysql

# install db
chmod +x /usr/local/mysql/scripts/mysql_install_db
/usr/local/mysql/scripts/mysql_install_db --defaults-file=/usr/local/mysql/my.cnf --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --user=mysql

chown -Rf mysql:mysql /usr/local/mysql

cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld


# start on up
chkconfig --add mysqld
chkconfig --level 345 mysqld on

# start mysql
/etc/init.d/mysqld start

# reset password
/usr/local/mysql/bin/mysqladmin -u root  password abc123

# check status
netstat -antp | grep :3306

echo "done!";
