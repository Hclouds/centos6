#!/bin/bash
#
# install_svn.sh - this script install subversion automatic
#
# chkconfig:   - 85 15
# description: how to install subversion soft
#

#install svn
yum -y install svn

#创建版本库所在的地方
mkdir -p /usr/local/svn

#创建一个项目
mkdir -p /usr/local/svn/abc

#初始化创建svn项目
svnadmin create /usr/local/svn/abc

#在abc目录下编辑文件,创建2个用户
vim /usr/local/snv/abc/conf/passwd #在[users]下添加
xudong7930=abc123


#编辑authz文件,分配权限
vim /usr/local/svn/abc/conf/authz #在[/]下添加
xudong7930=rw


#编辑配置文件
vim /usr/local/svn/abc/svnserve.conf #在[general]添加
anon-access=none
auth-access=write
password-db=passwd
authz-db=authz 



#启动和停止
svnserve -d -r /usr/local/svn
pkill -9 svnserve

#启动svn服务器
/etc/init.d/svnserve start
/etc/init.d/svnserve stop


客户端测试:
svn://192.168.1.12:3690/abc
